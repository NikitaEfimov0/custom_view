package com.example.task17.figures

import android.graphics.Canvas
import android.graphics.Color
import kotlin.random.Random

open class Shape {
     open var x:Float = 0f
        get() { return field }
        set(value){field = value}
     open var y:Float = 0f
        get() = field
        set(value){field = value}
    var colorOfShape: Int = Color.BLACK


    var ID:Int = 0;
    var type:String = ""
    fun setColor(){
        val c = Random.nextInt(1, 5)
        when (c){
            1->colorOfShape = Color.CYAN
            2->colorOfShape = Color.BLUE
            3->colorOfShape = Color.GREEN
            4->colorOfShape = Color.RED
            5->colorOfShape = Color.MAGENTA
        }
    }
    open fun draw(canvas: Canvas?){

    }

    open fun retXwidth():Float{
        return 0f
    }

    open fun retYheight():Float{
        return 0f
    }

    open fun setXwidth(f:Float){

    }

    open fun setYheight(f:Float){

    }

    open fun returnRad():Float{
        return 0f
    }
}