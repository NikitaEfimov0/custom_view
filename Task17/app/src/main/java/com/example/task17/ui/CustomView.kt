package com.example.task17.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import com.example.task17.AddNewObject
import com.example.task17.figures.Circle
import com.example.task17.figures.Rectangle
import com.example.task17.figures.Shape
import com.example.task17.figures.Square

class CustomView @JvmOverloads constructor(
    context: Context,
    attrs:AttributeSet?=null,
    defStyleAttr:Int = 0
):View(context, attrs, defStyleAttr), AddNewObject{
    val paint:Paint = Paint()
    private var iterForCir:Int = 0
    private var iterForSq:Int = 0
    private var iterForRec:Int = 0
    var listOfObjects:ArrayList<Shape> = ArrayList()



    var item:Shape? = null
    var position = 0

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
            for(i in listOfObjects){
                Log.d(i.type, "${i.x}, ${i.y}" )
                i.draw(canvas)
            }
    }

//    override fun performClick(): Boolean {
//        return super.performClick()
//    }
//

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val x = event?.x
        val y = event?.y

        when (event?.getAction()) {
            MotionEvent.ACTION_DOWN -> {
                for(i in listOfObjects.indices){
                    if(checkArea(x, y, listOfObjects[i])) {
                        item = listOfObjects[i]
                        position = i
                        break;
                    }
                }
            }
            MotionEvent.ACTION_MOVE -> {
                if(item!=null) {
                    redrawObject(item!!, position, x!!, y!!)
                }
            }
            MotionEvent.ACTION_UP->{
                item = null
                position = 0
            }
        }
        return true
    }

    override fun addCircle() {
        val circle:Circle = Circle()
        circle.setColor()
        circle.ID = "Circle$iterForCir".hashCode()
        circle.type = "Circle"
        iterForCir++
        listOfObjects.add(circle)
        //Toast.makeText(context, "Circle was added", Toast.LENGTH_SHORT).show()
        invalidate()
    }

    override fun addSquare() {
        val square:Square = Square()
        square.setColor()
        square.ID = "Square$iterForSq".hashCode()
        square.type = "Square"
        iterForSq++
        listOfObjects.add(square)
     //   Toast.makeText(context, "Square was added", Toast.LENGTH_SHORT).show()
        invalidate()
    }

    override fun addRectangle() {
        val rectangle:Rectangle = Rectangle()
        rectangle.setColor()
        rectangle.ID = "Rectangle$iterForRec".hashCode()
        rectangle.type = "Rectangle"
        iterForRec++
        listOfObjects.add(rectangle)
      //  Toast.makeText(context, "Square was added", Toast.LENGTH_SHORT).show()
        invalidate()
    }

    fun redrawObject(item:Shape, i:Int, x:Float, y:Float){
        if(item.type == "Circle"){
            item.x = x
            item.y = y
            listOfObjects[i] = item
        }
        else{
            item.x = x-((item.retXwidth()-item.x)/2)
            item.y = y - ((item.retYheight()-item.y)/2)
            item.setXwidth(x)
            item.setYheight(y)
            listOfObjects[i] = item

        }
        invalidate()
    }


    fun checkArea(x:Float?, y:Float?, sh:Shape):Boolean{
        if(sh.type == "Circle"){
            val newX = x!!-(sh.x)
            val newY = y!!-(sh.y)
            val xD = newX.times(newX)
            val yD = newY.times(newY)
            if((xD + yD)<=sh.returnRad().times(sh.returnRad()))
                return true
            return false
        }
        if(sh.type == "Square"){
            if(sh.x<=x!! && x <= sh.retXwidth() && sh.y<=y!! && y <sh.retYheight()){
                return true
            }
        }
        if(sh.type == "Rectangle"){
            if(sh.x<=x!! && x <= sh.retXwidth() && sh.y<=y!! && y <sh.retYheight()){
                return true
            }
        }

        return false
    }

    fun reset(){
        listOfObjects.clear()
        invalidate()
    }


}
