package com.example.task17.figures

import android.graphics.Canvas
import android.graphics.Paint

class Circle: Shape() {
    var radius:Float = 200f
    override var x = 200f
    override var y = 200f
    override fun draw(canvas: Canvas?){
        val paint:Paint = Paint()
        paint.apply {
            isAntiAlias = true
            color = colorOfShape
            style = Paint.Style.FILL
        }
        canvas?.drawCircle((x), (y), radius, paint)
    }

    override fun returnRad(): Float {
        return radius
    }

}