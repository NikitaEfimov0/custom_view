package com.example.task17

interface AddNewObject
{

    fun addCircle()
    fun addSquare()
    fun addRectangle()

}