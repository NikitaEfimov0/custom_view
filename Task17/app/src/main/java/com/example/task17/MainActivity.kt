package com.example.task17

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.widget.Button
import android.widget.Toast
import com.example.task17.ui.CustomView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    //val cV:CustomView = CustomView(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val CV:CustomView = findViewById(R.id.canvas)
        val addCirc:FloatingActionButton = findViewById(R.id.addCircle)
        val addSq:FloatingActionButton = findViewById(R.id.addSquare)
        val addRect:FloatingActionButton = findViewById(R.id.addRect)
        val reset:FloatingActionButton = findViewById(R.id.resetCanvas)
        addCirc.setOnClickListener {
            CV.addCircle()
        }

        addSq.setOnClickListener {
            CV.addSquare()
        }

        addRect.setOnClickListener {
            CV.addRectangle()
        }

        reset.setOnClickListener{
            CV.reset()
            Toast.makeText(this, "Reset successfully", Toast.LENGTH_LONG).show()
        }

    }
}