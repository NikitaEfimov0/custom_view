package com.example.task17.figures

import android.graphics.Canvas
import android.graphics.Paint

class Rectangle:Shape() {
    val width = 500f
    val height = 200f
    var widthX = width+x
    var heightY = height+y
    override fun draw(canvas: Canvas?){
        val paint: Paint = Paint()
        paint.apply {
            isAntiAlias = true
            color = colorOfShape
            style = Paint.Style.FILL
        }
        canvas?.drawRect(x, y, widthX, heightY, paint)
    }

    override fun retXwidth():Float{
        return widthX
    }

    override fun retYheight():Float{
        return heightY
    }

    override fun setXwidth(f:Float){
        widthX = x+width
    }

    override fun setYheight(f:Float){
        heightY = y+height
    }

}